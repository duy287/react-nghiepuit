import React, { Component } from 'react';
import './App.css';

import TaskForm from './components/TaskForm';
import Control from './components/Control';
import TaskList from './components/TaskList';

class App extends Component{
    constructor(props){
        super(props);
        this.state = {
            tasks: [],
            isDisplayForm: false,
            taskEditing: null,
            filter: {
                name: '',
                status: -1
            }
        }
    }
    UNSAFE_componentWillMount(){
        if(localStorage && localStorage.getItem('tasks')){
            var tasks = JSON.parse(localStorage.getItem('tasks'));
            this.setState({
                tasks: tasks
            })
        }
    }
    onGenerateData = () =>{
        var tasks = [
            {
                id: this.generateID(),
                name: 'AAA',
                status: true
            },
            {
                id: this.generateID(),
                name: 'BBB',
                status: false
            },
            {
                id: this.generateID(),
                name: 'CCC',
                status: true
            },
        ]
        this.setState({
            tasks: tasks
        })
        localStorage.setItem('tasks', JSON.stringify(tasks));
    }
    s4(){
        return Math.floor((1+ Math.random()) * 0x10000).toString(16).substring(1);
    }
    generateID(){
        return this.s4() + this.s4() + this.s4() + this.s4();
    }
    onToggleForm = ()=>{
        if(this.state.isDisplayForm && this.state.taskEditing !==null){
            this.setState({
                isDisplayForm: true,
                taskEditing: null
            });
        }
        else{
            this.setState({
                isDisplayForm: !this.state.isDisplayForm,
                taskEditing: null
            })
        }
    }
    onShowForm = ()=>{
        this.setState({
            isDisplayForm: true
        })
    }
    onSubmit = (data)=>{
        var {tasks} = this.state;
        if(data.id === ''){ // Trường hợp Thêm
            var task = {
                id: this.generateID(),
                name: data.name,
                status: data.status
            };
            tasks.push(task);
        }
        else{ // Trường hợp sửa
            var index = this.findIndex(data.id);
            tasks[index] = data;
        }

        this.setState({
            tasks: tasks,
            taskEditing: null
        });

        localStorage.setItem('tasks', JSON.stringify(tasks));
    }
    onUpdateStatus = (id) => {
        var {tasks} = this.state;
        var index = this.findIndex(id);
        if(index !== -1){
            tasks[index].status =  !tasks[index].status;
            this.setState({
                tasks: tasks
            })
            localStorage.setItem('tasks', JSON.stringify(tasks));
        }

    }
    findIndex = (id) => {
        var {tasks} = this.state;
        var result = -1;
        tasks.forEach((task, index)=>{
            if(task.id === id){
                result = index;
            }
        });
        return result;
    }
    onDelete = (id) =>{
        var {tasks} = this.state;
        var index = this.findIndex(id);
        if(index !== -1){
            tasks.splice(index, 1);
            this.setState({
                tasks: tasks
            })
            localStorage.setItem('tasks', JSON.stringify(tasks));
        }
    }
    onUpdate = (id)=>{
        var {tasks} = this.state;
        var index = this.findIndex(id);
        if(index !== -1){
            var taskEditing = tasks[index];
            this.setState({
                taskEditing: taskEditing
            })
            this.onShowForm();
        }
    }
    onFilter =(filterName, filterStatus)=>{
        filterStatus = parseInt(filterStatus);
        this.setState({
            filter : {
                name: filterName,
                status: filterStatus
            }
        });
    }
    render(){
        var {tasks, isDisplayForm, taskEditing, filter} = this.state; // <=> tasks = this.state.tasks
        //------------ Filter -----------//
        if(filter){
            if(filter.name){
                tasks = tasks.filter((task)=>{
                    return (task.name.toLowerCase().indexOf(filter.name) !== -1)
                })
            }
            tasks = tasks.filter((task)=>{
                if(filter.status=== -1)
                    return task;
                else
                    return task.status === (filter.status === 1? true: false);
            });
        }

        var elmTaskForm = isDisplayForm ? <TaskForm onSubmit={this.onSubmit} taskEditing={taskEditing}/> : '';
        return (
            <div className="container">
                <div className="row">
                    <div className={isDisplayForm? 'col-md-4': ''}>
                        {elmTaskForm}
                    </div>
                    <div className={isDisplayForm? 'col-md-8': 'col-md-12'}>
                        <button onClick={this.onToggleForm} className="btn btn-primary space-b">Thêm công việc</button>
                        <button className="btn btn-primary space-b space-l" onClick={this.onGenerateData}>Generate data</button>
                        <Control/>
                        <div className="card">
                            <div className="card-header">
                                Bảng dữ liệu
                            </div>
                            <div className="card-body">
                                <TaskList 
                                    tasks={tasks} 
                                    onUpdateStatus = {this.onUpdateStatus}
                                    onDelete = {this.onDelete}
                                    onUpdate = {this.onUpdate}
                                    onFilter = {this.onFilter}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
