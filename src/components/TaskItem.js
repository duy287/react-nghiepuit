import React, { Component } from 'react';

class TaskItem extends Component{
    onUpdateStatus = ()=>{
        this.props.onUpdateStatus(this.props.task.id);
    }
    onDelete = ()=>{
        this.props.onDelete(this.props.task.id);
    }
    onUpdate = ()=>{
        this.props.onUpdate(this.props.task.id);
    }
    render(){
        var {task, index} = this.props;
        return (
            <tr>
                <th scope="row">{index + 1}</th>
                <td>{task.name}</td>
                <td>
                    <span 
                        className={task.status===true? 'badge badge-success': 'badge badge-danger'}
                        onClick={this.onUpdateStatus}
                    >
                        {task.status===true? 'Kích hoạt': 'Ẩn'}
                    </span>
                </td>
                <td>
                    <button onClick={this.onDelete} className="btn btn-danger btn-sm" type="button">Delete</button>
                    <button onClick={this.onUpdate} className="btn btn-success btn-sm space-l" type="button">Edit</button>
                </td>
            </tr>
        );
    }
}

export default TaskItem;