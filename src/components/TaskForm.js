import React, { Component } from 'react';

class TaskForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            name: '',
            status: true,
            id: ''
        }
    }
    onChange = (event)=>{
        var target = event.target;
        var name = target.name;
        var value = target.value;
        if(name==='status'){
            value = target.value === 'true'? true : false;
        }
        this.setState({
            [name]:value
        });
    }
    onSubmit = (event)=>{
        event.preventDefault();
        this.props.onSubmit(this.state);
        this.onClear();
    }
    onClear = (e)=>{
        this.setState({
            name: '',
            status: false
        });
    }
    //-------------- Update ----------------
    UNSAFE_componentWillMount(){
        var {taskEditing} = this.props;
        if(taskEditing){
            this.setState({
                id: taskEditing.id,
                name: taskEditing.name,
                status: taskEditing.status
            });
        }
    }
    UNSAFE_componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.taskEditing){
            this.setState({
                id: nextProps.taskEditing.id,
                name: nextProps.taskEditing.name,
                status: nextProps.taskEditing.status
            });
        }
        else if(!nextProps.taskEditing){
            //Tương ứng trường hợp khi chuyển from từ Edit sang Add
            this.setState({
                id: '',
                name: '',
                status: false
            })
        }
    }
    render(){
        return (
            <div className="card">
                <div className="card-header">
                    {this.state.id !== '' ? 'Cập nhật công việc': 'Thêm công việc'}
                </div>
                <div className="card-body">
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label>Tên</label>
                            <input name="name" value={this.state.name} onChange={this.onChange} type="text" className="form-control" />
                        </div>
                        <div className="form-group">
                            <label>Trạng thái</label>
                            <select className="form-control" name="status" onChange={this.onChange} value={this.state.status}>
                                <option value={false}>An</option>
                                <option value={true}>Kich hoat</option>
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Lưu lại</button>
                        <button type="button" onClick={this.onClear} className="btn btn-danger space-l">Hủy bỏ</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default TaskForm;