import React, { Component } from 'react';

class Search extends Component{
    render(){
        return (
            <div className="input-group col-md-8">
                <input type="text" className="form-control"/>
                <div className="input-group-append">
                    <button className="btn btn-success" type="button">Search</button>
                </div>
            </div>
        );
    }
}

export default Search;