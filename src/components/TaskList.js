import React, { Component } from 'react';
import TaskItem from './TaskItem'

class TaskList extends Component{
    constructor(props){
        super(props);
        this.state = {
            filterName: '',
            filterStatus: -1 // all: -1, active: 1, deactive: 0
        }
    }
    onChange = (event)=>{
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.props.onFilter(
            name==='filterName' ? value : this.state.filterName,
            name==='filterStatus'? value : this.state.filterStatus
        );
        this.setState({
            [name]: value
        });
    }
    render(){
        var {tasks} = this.props;

        var elmTasks = tasks.map((task, index)=>{
            return <TaskItem 
                        key={task.id} 
                        index={index} 
                        task={task}
                        onUpdateStatus = {this.props.onUpdateStatus}
                        onDelete ={this.props.onDelete}
                        onUpdate ={this.props.onUpdate}
                    /> 
        })
        return (
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Trạng thái</th>
                        <th scope="col">Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th></th>
                        <td><input name="filterName" value={this.state.filterName} onChange={this.onChange} type="text" className="form-control" /></td>
                        <td>
                            <select name="filterStatus"  value={this.state.filterStatus} onChange={this.onChange} className="form-control">
                                <option value={-1}>All</option>
                                <option value={1}>Active</option>
                                <option value={0}>Deactive</option>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                    {elmTasks}
                </tbody>
            </table>
        );
    }
}

export default TaskList;